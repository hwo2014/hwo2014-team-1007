using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using hwo2014_zakuska;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

public class Bot {
	public static void Main(string[] args) {
   //     string host = args[0]; //testserver.helloworldopen.com hakkinen.helloworldopen.com (R2) senna.helloworldopen.com (R1)
   //     int port = int.Parse(args[1]); //8091
   //     string botName = args[2];//Zakuska
   //     string botKey = args[3]; //Q4c0jNUlp0jEtg
         string host = "testserver.helloworldopen.com"; //testserver.helloworldopen.com hakkinen.helloworldopen.com (R2) senna.helloworldopen.com (R1)
         int port = 8091; //8091
         string botName = "Zakuska";//Zakuska
         string botKey = "Q4c0jNUlp0jEtg"; //Q4c0jNUlp0jEtg

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;
    private CarPosClass carPosMsg;
    private GameInitClass gameInitMsg;
    private List<Piece> piecesList;

    String temp;
    int currentPieceIndex = 0;
    
    double currentThrottle = 0.62;
    List<int> switchPieces = new List<int>();
    List<int> straightPieces = new List<int>();
    List<int> lessSharpCurveLeft = new List<int>();
    List<int> lessSharpCurveRight = new List<int>();
    List<int> sharpCurveLeft = new List<int>(); // less than 0
    List<int> sharpCurveRight = new List<int>(); // greater than 0

    Boolean isLeftLane = true;
	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;

		send(join);

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch(msg.msgType) {
				case "carPositions":
                    //if a switchpiece is coming up soon, then find out, should the car switch? not working in the best way i think...
                    if (switchPieces.Contains(currentPieceIndex + 2))
                    {
                        switchLogic();
                    }
                    //strip square brackets from Json message with temp variable in order to be able to deserialize it with CarPosClass
                    temp = msg.data.ToString().Substring(1, msg.data.ToString().Length - 2);
                    carPosMsg = JsonConvert.DeserializeObject<CarPosClass>(temp);
                    currentPieceIndex =carPosMsg.piecePosition.pieceIndex;


                    Console.WriteLine("Piece index:" +currentPieceIndex); //shows current piece index
                    Console.WriteLine("Car angle:" +carPosMsg.angle); //shows if car is about to "slip" from the track
                    Console.WriteLine("Distance in piece:" +carPosMsg.piecePosition.inPieceDistance); //shows distance traveled in current piece.
                    Console.WriteLine("Throttle:" +currentThrottle); //shows current throttle

                    //create logic, set throttle with currentThrottle variable
                    /*
                     I tried making an AI that looks ahead, and checks which pieces were before it in order to 
                     * determine what to set the currentThrottle to but it wasnt that good...
                     * 
                     */
                    //the next few lines with currentPieceIndex are just temporary and should be changed...
                    if (currentPieceIndex == 34)
                    {
                        currentThrottle = 1.0;
                    }
                    else if (currentPieceIndex ==0 && currentThrottle==1.0)
                    {
                        currentThrottle = 0.45;
                    }
                    else if (currentPieceIndex >5 && currentThrottle == 0.45)
                    {
                        currentThrottle = 0.63;
                    }
                  
                    send(new Throttle(currentThrottle));
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine("Race init");
             //       Console.WriteLine(msg.data.ToString());
                    gameInitMsg = JsonConvert.DeserializeObject<GameInitClass>(msg.data.ToString());
                    piecesList = gameInitMsg.race.track.pieces;

                    int maxCount = piecesList.Count;
                    for (int index = 0; index < maxCount;index++ )
                    {
                        Console.WriteLine("Item length: " + piecesList[index].length);
                        Console.WriteLine("Item radius: " + piecesList[index].radius.ToString());
                        Console.WriteLine("Item angle: " + piecesList[index].angle.ToString());
                        Console.WriteLine("Item isSwitchPiece: " + piecesList[index].@switch.ToString());

                        //not sure if we need this but i was thinking it might be useful, in order to simplify methods that "look ahead"
                        if(index==maxCount-1)
                        {//add extra pieces in order to not have to check if last piece then go back to 0th index and check what type of piece it is
                            //max 4 pieces extra atm.
                            piecesList.Add(piecesList[0]);
                            piecesList.Add(piecesList[1]);
                            piecesList.Add(piecesList[2]);
                            piecesList.Add(piecesList[3]);
                        }
                        if (piecesList[index].@switch.Equals(true))
                        {
                            switchPieces.Add(index); //indexes of all switchPieces are in this list
                        }
                        if (piecesList[index].length > 0)
                        {
                            straightPieces.Add(index);//indexes of all straightPieces are in this list
                        }
                        else if (piecesList[index].angle > 0 && piecesList[index].angle < 30 )
                        {
                            lessSharpCurveRight.Add(index); //indexes of all Right curves with a low angle are in this list
                        }
                         else if (piecesList[index].angle < 0 && piecesList[index].angle > -30 )
                        {
                            lessSharpCurveLeft.Add(index);//indexes of all Left curves with a low angle are in this list
                        }
                        else if (piecesList[index].angle >= 30 )
                        {
                            sharpCurveRight.Add(index);//indexes of all Right curves with a higher angle are in this list
                        }
                        else if (piecesList[index].angle <= -30 )
                        {
                            sharpCurveLeft.Add(index);//indexes of all Left curves with a higher angle are in this list
                        }
                    }
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
                case "turboAvailable":
                    Console.WriteLine("!!!!!!!!!!!!!!Turbo is now available!!!!!!!!");
                    send(new Ping());
                    break;
                case "crash":
                    Console.WriteLine("Crashed");
                    break;
                case "spawn":
                    Console.WriteLine("Respawned");
                    break;
				default:
					send(new Ping());
					break;
			}

		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
    private void switchLogic()
    {
        //is a right curve coming up and are we on left lane? then switch lane on the swich piece that is coming up
        if (isLeftLane)
        {
            if(straightPieces.Contains(currentPieceIndex+2))
            {
                if(sharpCurveRight.Contains(currentPieceIndex+3) ||
                    lessSharpCurveRight.Contains(currentPieceIndex+3) || 
                    sharpCurveRight.Contains(currentPieceIndex+4) ||
                    lessSharpCurveRight.Contains(currentPieceIndex+4))
                {
                    send(new SwitchLane("Right"));
                }
            }
            else if(sharpCurveRight.Contains(currentPieceIndex+2))
            {
                send(new SwitchLane("Right"));
            }
            else if (lessSharpCurveRight.Contains(currentPieceIndex+2))
            {
                send(new SwitchLane("Right"));
            }
            isLeftLane = false;
        }
        else//if on right lane and left curve coming up switch to left lane
        {
            if (straightPieces.Contains(currentPieceIndex + 2))
            {
                if (sharpCurveLeft.Contains(currentPieceIndex + 3) ||
                    lessSharpCurveLeft.Contains(currentPieceIndex + 3) ||
                    sharpCurveLeft.Contains(currentPieceIndex + 4) ||
                    lessSharpCurveLeft.Contains(currentPieceIndex + 4))
                {
                    send(new SwitchLane("Left"));
                }
            }
            else if (sharpCurveLeft.Contains(currentPieceIndex + 2))
            {
                send(new SwitchLane("Left"));
            }
            else if (lessSharpCurveLeft.Contains(currentPieceIndex + 2))
            {
                send(new SwitchLane("Left"));
            }
            isLeftLane = true;
        }
    }
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}


abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class SwitchLane : SendMsg
{
    public string switchLaneMsg;

    public SwitchLane(string switchLaneMsg)
    {
        this.switchLaneMsg = switchLaneMsg;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }

    protected override Object MsgData()
    {
        return this.switchLaneMsg;
    }
}
class Turbo : SendMsg
{
    public string turboMessage;

    public Turbo(string turboMessage)
    {
        this.turboMessage = turboMessage;
    }

    protected override string MsgType()
    {
        return "turbo";
    }

    protected override Object MsgData()
    {
        return this.turboMessage;
    }
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}
