﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hwo2014_zakuska
{
    public class CarPosClass
    {
        public Id id { get; set; }
        public double angle { get; set; }
        public PiecePosition piecePosition { get; set; }

        public String name{ get; set; }
        public String color { get; set; }
        public class Id
        {
            public string name { get; set; }
            public string color { get; set; }
        }

public class Lane
{
    public int startLaneIndex { get; set; }
    public int endLaneIndex { get; set; }
}

public class PiecePosition
{
    public int pieceIndex { get; set; }
    public double inPieceDistance { get; set; }
    public Lane lane { get; set; }
    public int lap { get; set; }
}

    }
}
